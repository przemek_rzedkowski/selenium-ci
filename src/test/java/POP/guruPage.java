package POP;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import utils.Log;

public class guruPage extends BasePage{

    public WebDriver driver;

    public guruPage(WebDriver driver) {
        //System.out.println(driver);
        this.driver = driver;
    }

    public void getUrl() {
        Log.info("Acquired guru page");
        driver.get("http://demo.guru99.com/test/newtours/");
    }

    public Boolean getTitle() {
        Log.warn("Getting guru page title");
        return driver.getTitle().contentEquals("Welcome: Mercury Tours");
    }

    public Boolean getHeadline() {
        Log.debug("Getting some element");
        try {
            driver.findElement(By.xpath("//a[@class='dropdown-toggle'][contains(text(),'Srelenium')]")).isDisplayed();
        } catch (NoSuchElementException e) {
            Log.error("No element found!");
            return false;
        }
        Log.error("Element found!");
        return true;
    }
}
