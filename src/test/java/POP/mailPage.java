package POP;

import com.mailslurp.api.api.CommonActionsControllerApi;
import com.mailslurp.api.api.InboxControllerApi;
import com.mailslurp.api.api.WaitForControllerApi;
import com.mailslurp.client.ApiClient;
import com.mailslurp.client.ApiException;
import com.mailslurp.models.Email;
import com.mailslurp.models.Inbox;
import com.mailslurp.models.SendEmailOptions;
import org.openqa.selenium.WebDriver;

import java.util.Collections;
import java.util.UUID;

public class mailPage {

    private static final String YOUR_API_KEY = "546e053fb4778acce7b7c2c4b7b101770ecca979dbe3d816706b610b31df2716";

    // set a timeout as fetching emails might take time
    private static final Long TIMEOUT_MILLIS = 30000L;
    private static final boolean UNREAD_ONLY = true;

    private final ApiClient mailslurpClient;
    private final InboxControllerApi inboxControllerApi;
    private Email email;
    public WebDriver driver;

    public mailPage(WebDriver driver) {
        this.driver = driver;
        this.mailslurpClient = com.mailslurp.client.Configuration.getDefaultApiClient();
        this.mailslurpClient.setApiKey(YOUR_API_KEY);
        this.mailslurpClient.setConnectTimeout(TIMEOUT_MILLIS.intValue());
        this.inboxControllerApi = new InboxControllerApi(mailslurpClient);
    }

    public void sendMessage() throws ApiException {
        Inbox inbox = inboxControllerApi.getInbox(UUID.fromString("55aa9bbe-d585-45c9-b487-54a110526c89"));

        System.out.println(inbox.getEmailAddress());
        SendEmailOptions sendEmailOptions = new SendEmailOptions()
                .to(Collections.singletonList("c75c4ecf-32d2-44c5-9261-025a52d75b6c@mailslurp.com"))
                .subject("Test3")
                .body("Hello bitch");
        inboxControllerApi.sendEmail(inbox.getId(), sendEmailOptions);
    }

    public void getMessages() throws ApiException {
        Inbox inbox2 = inboxControllerApi.getInbox(UUID.fromString("c75c4ecf-32d2-44c5-9261-025a52d75b6c"));
        WaitForControllerApi waitForControllerApi = new WaitForControllerApi(mailslurpClient);

        email = waitForControllerApi
                .waitForLatestEmail(inbox2.getId(), TIMEOUT_MILLIS, UNREAD_ONLY);
        System.out.println(email.getSubject());
    }

    public void getMessageText() {
        System.out.println(email.getBody());
    }

    public void clearInbox2() throws ApiException {
        CommonActionsControllerApi commonActionsControllerApi = new CommonActionsControllerApi(mailslurpClient);
        commonActionsControllerApi.emptyInbox(UUID.fromString("c75c4ecf-32d2-44c5-9261-025a52d75b6c"));
    }
}
