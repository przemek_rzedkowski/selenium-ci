package POP;

import org.openqa.selenium.WebDriver;
import utils.Log;

public class travelsPage extends BasePage{

    private WebDriver driver;

    public travelsPage(WebDriver driver) {
        //System.out.println(driver);
        this.driver = driver;
    }

    public void getUrl() {
        driver.get("http://www.kurs-selenium.pl/demo/");
        Log.info("Travels page opened");
    }

    public Boolean getTitle() {
        Log.info("Acquiring travels page title");
        return driver.getTitle().contentEquals("PHPTRAVELS | Travel Technology Partner");
    }
}
