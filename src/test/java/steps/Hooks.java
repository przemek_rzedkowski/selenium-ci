package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.ThreadContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.Log;


public class Hooks {

    //TestBase testBase = new TestBase();

    @Before
    public void start(Scenario scenario) {
        ThreadContext.put("ROUTINGKEY", Thread.currentThread().getName());
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox", "--disable-dev-shm-usage", "window-size=1024,768");
        chromeOptions.addArguments("--headless");
        TestBase.driver.set(new ChromeDriver(chromeOptions));
        Log.info("------------- " + scenario.getName() + " has started -------------");
        //System.out.println("before");
    }

    @After
    public void end(Scenario scenario) {
        //System.out.println("afterek");
        byte src[] = ((TakesScreenshot)TestBase.driver.get()).getScreenshotAs(OutputType.BYTES);
        scenario.attach(src, "image/png", "");
        TestBase.driver.get().quit();
        Log.info("------------- " + scenario.getName() + " has ended -------------\n");
        ThreadContext.remove("ROUTINGKEY");
    }
}
