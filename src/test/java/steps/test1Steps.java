package steps;


import POP.guruPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class test1Steps extends TestBase {

    POP.guruPage guruPage = new guruPage(driver.get());

    @Given("^User opens demo guru page$")
    public void opens() {
        //System.out.println(TestBase.driver);
        guruPage.getUrl();
    }

    @Then("^User sees demo guru title$")
    public void userSeesDemoGuruTitle() {
        System.out.println("test 1 method 1");
        guruPage.getTitle();
    }

    @Then("User sees headline")
    public void userSeesHeadline() throws InterruptedException {
        System.out.println("test 1 method 2");
        Thread.sleep(5000);
        guruPage.getHeadline();
    }
}
