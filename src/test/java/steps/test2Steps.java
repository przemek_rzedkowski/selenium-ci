package steps;


import POP.travelsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;


public class test2Steps extends TestBase {

    POP.travelsPage travelsPage = new travelsPage(driver.get());

    @Given("^User opens travels page$")
    public void opens() {
        travelsPage.getUrl();
    }

    @Then("^User sees travels page title$")
    public void userSeesDemoGuruTitle() {
        System.out.println("test 2 method 1");
        travelsPage.getTitle();
    }
}
