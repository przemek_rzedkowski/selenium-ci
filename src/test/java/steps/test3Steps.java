package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;


public class test3Steps extends TestBase {

    private Response response;

    @Given("^User sent response successfully$")
    public void getResponse() {
        response = given().when().get("https://jsonplaceholder.typicode.com/users")
                .then().extract().response();
    }

    @Then("^User sees username Bret$")
    public void validateResponse() {
        assertEquals(200, response.getStatusCode());
        assertEquals("Bret", response.jsonPath().getList("username").get(0));
    }
}
