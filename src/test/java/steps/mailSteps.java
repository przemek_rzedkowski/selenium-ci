package steps;

import POP.mailPage;
import com.mailslurp.client.ApiException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.Log;


public class mailSteps extends TestBase {

    POP.mailPage mailPage = new mailPage(driver.get());

    @Given("^User sends email$")
    public void userSendsEmail() throws ApiException {
        Log.info("Sending message");
        mailPage.sendMessage();
    }

    @When("User opens inbox")
    public void userOpensInbox() throws ApiException {
        Log.info("Opening inbox");
        mailPage.getMessages();
    }

    @Then("User sees email text")
    public void userSeesEmailText() {
        Log.info("Getting email content");
        mailPage.getMessageText();
    }

    @And("User clears inbox")
    public void userClearsInbox() throws ApiException {
        Log.info("Clearing inbox");
        mailPage.clearInbox2();
    }
}
