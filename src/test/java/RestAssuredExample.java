
import java.util.List;

import static io.restassured.RestAssured.given;

public class RestAssuredExample {

    private static String url = "http://demo.guru99.com/V4/sinkministatement.php?CUSTOMER_ID=68195&PASSWORD=1234!&Account_No=1";

    public static void main(String[] args) {
        getResponseBody();
        getResponseStatus();
        getResponseHeaders();
        getResponseContentType();
        getSpecificPartOfResponseBody();
    }

    public static void getResponseBody(){
        given().queryParam("CUSTOMER_ID","68195")
                .queryParam("PASSWORD","1234!")
                .queryParam("Account_No","1")
                .when().get("http://demo.guru99.com/V4/sinkministatement.php").then().log()
                .body();
    }

    public static void getResponseStatus(){
        int statusCode= given().queryParam("CUSTOMER_ID","68195")
                .queryParam("PASSWORD","1234!")
                .queryParam("Account_No","1") .when().get("http://demo.guru99.com/V4/sinkministatement.php").getStatusCode();
        System.out.println("The response status is " +statusCode);

        given().when().get(url).then().assertThat().statusCode(200);
    }

    public static void getResponseHeaders(){
        System.out.println("The headers in the response "+
                given().when().get(url).then().extract()
                        .headers());
    }

    public static void getResponseContentType(){
        System.out.println("The content type of response "+
                given().when().get(url).then().extract()
                        .contentType());
    }

    public static void getSpecificPartOfResponseBody(){
        List<String> amounts = given().when().get("https://jsonplaceholder.typicode.com/users")
                                    .then().extract().response().jsonPath().getList("username");
        System.out.println(amounts.get(0));

    }
}
