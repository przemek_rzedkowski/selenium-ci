# Selenium Java Framework

This is my test framework based on Selenium and Java.  

## Table of contents 
* [General info](#general-info) 
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage](#usage)
* [Features covered](#features-covered)
* [TODOs](#todos)
* [Other info](#other-info)

### General info

Said framework came into existence as a response to my personal and professional needs.
I was tired of using someone's ideas with all their pros and cons. I wanted to have my own cons :) 
 
### Technologies

* JDK v. 11.0.8
* Maven v. 3.1.0
* Cucumber v. 6.8.0
* Maven Surefire v. 3.0.0-M5
* Mailslurp v. 7.0.11-RELEASE
* Selenium v. 3.141.59
* Extent Reports Cucumber Adapter v. 1.2.0
* Log4j v. 2.13.3

### Setup

Java 12 is recommended. Although it should work for earlier releases. 

Ensure Java is installed properly and **$JAVA_HOME** is set:

**For macOS and Linux machines:**

```
$ java --version
java 12.0.2 2019-07-16
Java(TM) SE Runtime Environment (build 12.0.2+10)
Java HotSpot(TM) 64-Bit Server VM (build 12.0.2+10, mixed mode, sharing)
$ echo $JAVA_HOME
/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin:/usr/libexec/java_home
```

Then check if maven is installed properly:
```
$ mvn --version
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /usr/local/Cellar/maven/3.6.3_1/libexec
Java version: 14.0.1, vendor: N/A, runtime: /usr/local/Cellar/openjdk/14.0.1/libexec/openjdk.jdk/Contents/Home
Default locale: pl_PL, platform encoding: UTF-8
OS name: "mac os x", version: "10.16", arch: "x86_64", family: "mac"
```

**For Microsoft Windows based machines:**
```
$ java -version
java version "1.8.0_261"
Java(TM) SE Runtime Environment (build 1.8.0_261-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.261-b12, mixed mode)
$ echo %JAVA_HOME%
X:\Program Files\Java\jdk-11.0.8
```

Then check if maven is installed properly:
```
$ mvn -version
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: C:\Program Files\apache-maven-3.6.3\bin\..
Java version: 11.0.8, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-11.0.8
Default locale: pl_PL, platform encoding: Cp1250
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

If any of this commands returns blank or any requirements is not fulfilled missing install it accordingly.\
[How to install java?](https://docs.oracle.com/javase/10/install/installation-jdk-and-jre-macos.htm#JSJIG-GUID-F575EB4A-70D3-4AB4-A20E-DBE95171AB5F)\
[How to install maven?](https://maven.apache.org/install.html)

Alternatively tests might be run directly from IDE by running **TestRunner** class(here is screenshot from IntelliJ IDEA):
![Running](docs/run.PNG)

### Usage

Each feature file (file with `.feature` extension) represents another feature from AUT.
Classes located in `steps` directory are middle layer between actual logic (classes from `POP`) and feature files.
For more information about Page Object test design pattern start [here](https://www.selenium.dev/documentation/en/guidelines_and_recommendations/page_object_models/).
`Utils` contains so called _helpers_ classes - they are here to help write logic fast and minimise code multiplication.
Log files (`logs`) and html report (`SparkReport`) can be found in `test-output`. Note that there will be separate log file for each thread.
Last but not least various configuration files can be found in `resources` directory.

Other important files:
* BasePage - base class for other _Page_ classes to extend
* Hooks - basic test configuration
* TestBase - base class for _Steps_ classes to extend 
* TestRunner - cucumber test runner
* .gitlab-ci.yml - gitlab pipeline script
* RestAssuredExample - example of Rest Assured test  
* pom.xml - [here](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)

Test suite can be run twofold using maven - simple `mvn test` will run test suite.


### Features covered

- [x] BDD
- [x] testNG example
- [x] parallel test execution (Surefire + junit)
- [x] reporting (Extent)
- [x] test logs
- [x] POP
- [x] running in pipeline
- [x] mail tests (external service)

### TODOs

- [ ] visual tests
- [ ] mail tests using SMTP
- [ ] custom waits
- [ ] docker image of tests

### Other info

In case of any additional questions/feedback feel free to contact me via [LinkedIn](https://www.linkedin.com/in/przemys%C5%82aw-rzedkowski-8029a8126/)
